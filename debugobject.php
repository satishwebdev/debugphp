<?php
/**
 * Determines current server API, ie, are we running from commandline or webserver.
 *
 * @return boolean
 */
function isRunningFromCLI()
{
    // STDIN isn't a CLI constant before 4.3.0
    $sapi = php_sapi_name();
    if (version_compare(PHP_VERSION, '4.3.0') >= 0 && $sapi != 'cgi') {
        if (!defined('STDIN')) {
            return false;
        } else {
            return @is_resource(STDIN);
        }
    } else {
        return in_array($sapi, array('cli', 'cgi')) && empty($_SERVER['REMOTE_ADDR']);
    }
}

function registerTrustedIPs()
{
    if (!empty($GLOBALS['_SGL']['TRUSTED_IPS'])) {
        return;
    }
    // Only IPs defined here can access debug sessions and delete config files
    $GLOBALS['_SGL']['TRUSTED_IPS']     = array('127.0.0.1');
    $GLOBALS['_SGL']['TRUSTED_IPS'][]   = '::1';
    $GLOBALS['_SGL']['TRUSTED_IPS'][]   = '10.0.*.*'; // All IPs in LAN
    $GLOBALS['_SGL']['TRUSTED_IPS'][]   = '192.168.*.*'; // All IPs in LAN
    // 
    $GLOBALS['_SGL']['TRUSTED_IPS'][]   = '107.23.78.104'; // TR DEV
}

function isTrustedIP($allowCli = false)
{
    $isTrustedIP = false;
    registerTrustedIPs();

    if (in_array($_SERVER['SERVER_NAME'], array(
        'localhost',
        '127.0.0.1'
    ))) {
        return true;
    }

    if (isRunningFromCLI()) {
        return true;
    }

    if (empty($_SERVER['REMOTE_ADDR'])) {
        return false;
    }

    if (!empty($GLOBALS['_SGL']['TRUSTED_IPS'])) {
        foreach ($GLOBALS['_SGL']['TRUSTED_IPS'] as $trustedIP) {
            if (@preg_match("/^$trustedIP$/", $_SERVER['REMOTE_ADDR'], $aMatches)) {
                return true;
            }
        }
    }
    return $isTrustedIP;
}

function debugObject($obj = null, $header = '', $exit = false)
{
    $isTrustedIP = isTrustedIP();
    $isRunningFromCLI = isRunningFromCLI();
    $strSeparator = "\n" . str_repeat('=', 80);
    if (!$isTrustedIP) {
        return false;
    }
    if (!function_exists('htmlspecialchars_recursive')) {
        function htmlspecialchars_recursive($obj = null)
        {
            if (is_array($obj)) {
                foreach ($obj as $key => $value) {
                    $obj[$key] = htmlspecialchars_recursive($value);
                }
            } elseif (is_object($obj)) {
                $aObjectVars = get_object_vars($obj);
                foreach ($aObjectVars as $key => $value) {
                    $obj->$key = htmlspecialchars_recursive($value);
                }
            } elseif (is_scalar($obj)) {
                $obj = htmlspecialchars($obj);
            }
            return $obj;
        }
    }

    $headerHtml = $header;
    if (!$isRunningFromCLI && $header && strpos($header, '<h2>', 0) === false
            && strpos($header, '<h3', 0) === false) {
        $headerHtml = "<h2>{$header}</h2>";
    }

    // Obtain backtrace information, if supported by PHP
    $backtraceInfo = '';
    if (!$isRunningFromCLI && version_compare(phpversion(), '4.3.0') >= 0) {
        $bt = debug_backtrace();
        $backtraceInfo .= 'Fired from ';
        if (isset($bt[1]['class'])
            && $bt[1]['type']
            && isset($bt[1]['function'])) {
            $backtraceInfo .= 'Method::' . $bt[1]['class'] . $bt[1]['type']
                . $bt[1]['function'];
        } elseif (isset($bt[1]['function'])) {
            $backtraceInfo .= 'Function::' . $bt[1]['function'];
        }
        if (isset($bt[0]['file']) && isset($bt[0]['line'])) {
            $backtraceInfo .= " line " . $bt[0]['line'] . " of \n\"" . $bt[0]['file'] . '"';
        }
        $headerHtml .= "\n" . '<h3 style="border-bottom: 1px dashed #805E42;">'
            . $backtraceInfo . '</h3>';
        $header .= "\n" . $backtraceInfo . "\n";
    }

    if ($isRunningFromCLI) {
        if ($header) {
            echo $strSeparator;
            echo "\n" . $header;
            echo $strSeparator;
        }
        if (!is_scalar($obj) || $obj != '') {
            echo "\n";
            print_r($obj);
        }
        flush();
    } else {
        echo "\n";
        echo '<pre style="
            background-color: #FAFAFA;
            border: 1px solid #BBBBBB;
            text-align: left;
            font-size: 9pt;
            line-height: 125%;
            margin: 0.5em 1em 1.8em;
            overflow: auto;
            padding: 0.99em;">';
        echo $headerHtml;
        echo "\n";
        // $type = intGetVariableType($obj);
        // echo "<h4>Object Type: $type</h4>";
        print_r(htmlspecialchars_recursive($obj));
        echo "\n</pre>";
    }

    if (is_callable(array('Fetise', 'logMessage'))) {
        $message = print_r($obj, true);
        if (!empty($header)) {
            $message = $header . $message;
        }
        Fetise::logMessage($message);
    }

    if ($exit) {
        exit();
    }
}

function debugObjectType($obj = null, $header = '', $exit = false)
{
    //Build Header
    if ($header) {
        $header = "<h2>{$header}</h2>";
    }

    // Obtain backtrace information, if supported by PHP
    $backtraceInfo = '';
    if (version_compare(phpversion(), '4.3.0') >= 0) {
        $bt = debug_backtrace();
        $backtraceInfo .= 'Fired from ';
        if (isset($bt[1]['class'])
            && $bt[1]['type']
            && isset($bt[1]['function'])) {
            $backtraceInfo .= 'Method::' . $bt[1]['class'] . $bt[1]['type']
                . $bt[1]['function'];
        } elseif (isset($bt[1]['function'])) {
            $backtraceInfo .= 'Function::' . $bt[1]['function'];
        }
        if (isset($bt[0]['file']) && isset($bt[0]['line'])) {
            $backtraceInfo .= " line " . $bt[0]['line'] . " of \n\"" . $bt[0]['file'] . '"';
        }
        $backtraceInfo = '<h3 style="border-bottom: 1px dashed #805E42;">'
            . $backtraceInfo . '</h3>';
    }
    $header .= $backtraceInfo . "\n";

    debugObject(intGetVariableType($obj), $header, $exit);
}

function debugObjectSafe($obj = null, $header = '', $exit = false)
{
    if (!isTrustedIP()) {
        return false;
    }

    //Build Header
    if ($header) {
        $header = "<h2>{$header}</h2>";
    }

    // Obtain backtrace information, if supported by PHP
    $backtraceInfo = '';
    if (version_compare(phpversion(), '4.3.0') >= 0) {
        $bt = debug_backtrace();
        $backtraceInfo .= 'Fired from ';
        if (isset($bt[1]['class'])
            && $bt[1]['type']
            && isset($bt[1]['function'])) {
            $backtraceInfo .= 'Method::' . $bt[1]['class'] . $bt[1]['type']
                . $bt[1]['function'];
        } elseif (isset($bt[1]['function'])) {
            $backtraceInfo .= 'Function::' . $bt[1]['function'];
        }
        if (isset($bt[0]['file']) && isset($bt[0]['line'])) {
            $backtraceInfo .= " line " . $bt[0]['line'] . " of \n\"" . $bt[0]['file'] . '"';
        }
        $backtraceInfo = "<h3>$backtraceInfo</h3>";
    }
    $header .= $backtraceInfo . "\n";

    echo "\n";
    echo '<pre style="
        background-color: #FAFAFA;
        border: 1px solid #BBBBBB;
        text-align: left;
        font-size: 9pt;
        line-height: 125%;
        margin: 0.5em 1em 1.8em;
        overflow: auto;
        padding: 0.99em;">';
    echo $header;
    print_r($obj);
    echo "\n</pre>";
    if ($exit) {
        exit();
    }
}

function intGetVariableType($obj)
{
    $type = 'unknown';
    if (is_object($obj)) {
        return get_class($obj);
    } elseif (is_resource($obj)) {
        return get_resource_type($obj);
    } elseif (is_array($obj)) {
        return 'Array';
    } elseif (is_numeric($obj)) {
        return 'Number';
    } elseif (is_string($obj)) {
        return 'String';
    }
    return $type;
}

function debugBacktraceInfo($maxLevel = 90, $header = '')
{
    $bt = debug_backtrace();
    $aDebugObject = array();
    $level = 0;
    foreach ($bt as $btInfo) {
        if ($level > $maxLevel) {
            break;
        }
        $aBTInfo = array(
            'file'      => $btInfo['file'],
            'line'      => $btInfo['line'],
            'function'  => @$btInfo['function'],
            'class'     => @$btInfo['class'],
            'type'      => @$btInfo['type'],
        );
        $aDebugObject[] = $aBTInfo;
        $level++;
    }

    // Obtain backtrace information, if supported by PHP
    $backtraceInfo = '';
    if (version_compare(phpversion(), '4.3.0') >= 0) {
        $bt = debug_backtrace();
        $backtraceInfo .= 'Fired from ';
        if (isset($bt[1]['class'])
            && $bt[1]['type']
            && isset($bt[1]['function'])) {
            $backtraceInfo .= 'Method::' . $bt[1]['class'] . $bt[1]['type']
                . $bt[1]['function'];
        } elseif (isset($bt[1]['function'])) {
            $backtraceInfo .= 'Function::' . $bt[1]['function'];
        }
        if (isset($bt[0]['file']) && isset($bt[0]['line'])) {
            $backtraceInfo .= " line " . $bt[0]['line'] . " of \n\"" . $bt[0]['file'] . '"';
        }
        $backtraceInfo = '<h3 style="border-bottom: 1px dashed #805E42;">'
            . $backtraceInfo . '</h3>';
    }
    $header .= $backtraceInfo . "\n";

    debugObject($aDebugObject, $header);
}

?>
